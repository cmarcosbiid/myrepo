package com.adaptionsoft.games.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

import com.adaptionsoft.games.uglytrivia.Game;

public class TrivaTest {

	@Test
	public void isGamePlayableTest() {
		Game gameNoPlayer = new Game();
		assertFalse("Minimum players are 2", gameNoPlayer.isPlayable());

		Game gameTwoPlayers = new Game();
		gameTwoPlayers.add("First player");
		gameTwoPlayers.add("Second player");

		assertTrue("Two players should be enough to play", gameTwoPlayers.isPlayable());

	}

	@Test
	public void addPlayersTest() {
		Game game = new Game();

		game.add("First player");
		assertEquals("Number of players shoud be 1", 1, game.howManyPlayers());

		game.add("Second player");
		assertEquals("Number of players shoud be 2", 2, game.howManyPlayers());

		game.add("Third player");
		assertEquals("Number of players shoud be 3", 3, game.howManyPlayers());

	}

	@Test
	public void deletePlayersTest() {
		Game game = new Game();

		game.add("First player");
		assertEquals("Number of players shoud be 1", 1, game.howManyPlayers());

		game.add("Second player");
		assertEquals("Number of players shoud be 2", 2, game.howManyPlayers());

		game.add("Third player");
		assertEquals("Number of players shoud be 3", 3, game.howManyPlayers());

		game.remove("Third player");
		assertEquals("Number of players should be 2", 2, game.howManyPlayers());

	}

	@Test
	public void multiplePlayersTest() {

		Game game = new Game();
		Random rand = new Random();

		int numberOfPlayers = rand.nextInt(100) + 6;

		for (int i = 1; i <= numberOfPlayers; i++) {
			game.add("Player " + i + 1);
			assertEquals("Number of players should be ", i, game.howManyPlayers());
		}
	}

	@Test
	public void playerWinTest() {
		Game game = new Game();
		game.add("First player");
		game.add("Second player");

		Random rand = new Random();
		boolean winner = false;

		for (int i = 0; i < 11; i++) {
			game.roll(rand.nextInt(5) + 1);
			winner = game.wasCorrectlyAnswered();
		}

		assertFalse("Player one has to win", winner);

	}

	@Test
	public void penaltyBoxTest() {
		Game game = new Game();
		game.add("First player");
		game.add("Second player");

		Random rand = new Random();
		boolean winner1 = false;
		boolean winner2 = false;

		for (int i = 0; i < 11; i++) {
			game.roll(rand.nextInt(6));
			if (i % 2 != 0)
				winner1 = game.wrongAnswer();
			else
				winner2 = game.wasCorrectlyAnswered();
		}

		assertFalse("Player one has to win", winner2);

	}

	@Test
	public void rollBiggerThanSixTest() {
		Game game = new Game();
		game.add("First player");
		game.add("Second player");

		Random rand = new Random();
		boolean winner = false;

		for (int i = 0; i < 11; i++) {
			game.roll(rand.nextInt(5) + 6);
			winner = game.wasCorrectlyAnswered();
		}

		assertFalse("Player one has to win", winner);

	}

	@Test
	public void releasePlayerTest() {
		Game game = new Game();
		game.add("First player");
		game.add("Second player");
		game.add("Third player");

		Random rand = new Random();
		boolean winner = false;

		for (int i = 0; i <= 18; i++) {
			game.roll(rand.nextInt(5) + 1);
			if (game.getCurrentPlayer() == 0)
				if (i == 15)
					game.remove("First player");
				else
					winner = game.wasCorrectlyAnswered();
			else if (game.getCurrentPlayer() == 1)
				winner = game.wrongAnswer();
			else
				winner = game.wasCorrectlyAnswered();
		}

		assertEquals("Player 3 has to win.", 2, game.getWinner());

	}

}
