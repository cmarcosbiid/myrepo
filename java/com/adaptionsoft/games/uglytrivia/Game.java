package com.adaptionsoft.games.uglytrivia;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.adaptionsoft.games.uglytrivia.entity.Player;
import com.adaptionsoft.games.uglytrivia.entity.PopQuestion;
import com.adaptionsoft.games.uglytrivia.entity.RockQuestion;
import com.adaptionsoft.games.uglytrivia.entity.ScienceQuestion;
import com.adaptionsoft.games.uglytrivia.entity.SportsQuestion;

public class Game {

	private ArrayList<Player> players = new ArrayList<Player>();

	private List<Integer> places = new ArrayList<Integer>();
	private List<Integer> purses = new ArrayList<Integer>();
	private List<Boolean> inPenaltyBox = new ArrayList<Boolean>();
	private List<Boolean> highscores = new ArrayList<Boolean>();

	private LinkedList<PopQuestion> popQuestions = new LinkedList<PopQuestion>();
	private LinkedList<ScienceQuestion> scienceQuestions = new LinkedList<ScienceQuestion>();
	private LinkedList<SportsQuestion> sportsQuestions = new LinkedList<SportsQuestion>();
	private LinkedList<RockQuestion> rockQuestions = new LinkedList<RockQuestion>();

	private int currentPlayer = 0;
	private boolean isGettingOutOfPenaltyBox;

	public enum Category {
		POP, SCIENCE, SPORTS, ROCK;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	public Game() {
		generateRandomQuestions();
	}

	public boolean add(String playerName) {

		players.add(new Player(playerName));

		places.add(0);
		purses.add(0);
		inPenaltyBox.add(false);

		System.out.println(playerName + " was added");
		System.out.println("They are player number " + players.size());

		return true;
	}

	/**
	 * @return true if the game is playable.
	 */
	public boolean isPlayable() {
		return (howManyPlayers() >= 2);
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public int getWinner() {

		for (Player player : players) {
			if (player.isActive() && purses.get(players.indexOf(player)) == 6)
				return players.indexOf(player);
		}

		return -1;
	}

	public boolean remove(String playerName) {

		boolean removed = false;

		if (howManyPlayers() >= 3) {

			int index = players.indexOf(new Player(playerName));

			if (index != -1) {
				Player player = players.get(index);
				player.deactivate();
				players.set(index, player);

				System.out.println("Player " + player + " was removed from the game");

				if (index == currentPlayer)
					nextPlayer();

				removed = true;
			} else {
				removed = false;
			}
		}

		return removed;

	}

	public int howManyPlayers() {

		int numberOfPlayers = 0;

		for (Player player : players) {
			if (player.isActive())
				numberOfPlayers++;
		}

		return numberOfPlayers;

	}

	public void roll(int roll) {
		System.out.println(players.get(currentPlayer) + " is the current player");
		System.out.println("They have rolled a " + roll);

		if (inPenaltyBox.get(currentPlayer)) {
			if (roll % 2 != 0) {
				isGettingOutOfPenaltyBox = true;
				System.out.println(players.get(currentPlayer) + " is getting out of the penalty box");

				moveRoll(roll);
				askQuestion();
			} else {
				System.out.println(players.get(currentPlayer) + " is not getting out of the penalty box");
				isGettingOutOfPenaltyBox = false;
			}

		} else {
			moveRoll(roll);
			askQuestion();
		}

	}

	/**
	 * 
	 * @return false if it is a winner
	 */
	public boolean wasCorrectlyAnswered() {
		if (inPenaltyBox.get(currentPlayer)) {
			if (isGettingOutOfPenaltyBox) {
				return isNotWinner();

			} else {
				nextPlayer();
				return true;
			}

		} else {

			return isNotWinner();

		}
	}

	public boolean wrongAnswer() {
		System.out.println("Question was incorrectly answered");
		System.out.println(players.get(currentPlayer) + " was sent to the penalty box");

		inPenaltyBox.set(currentPlayer, true);

		nextPlayer();
		return true;
	}

	private boolean isNotWinner() {
		System.out.println("Answer was correct!!!!");
		purses.set(currentPlayer, purses.get(currentPlayer) + 1);
		System.out.println(players.get(currentPlayer) + " now has " + purses.get(currentPlayer) + " Gold Coins.");

		boolean winner = didPlayerWin();
		nextPlayer();
		return winner;
	}

	private void nextPlayer() {

		currentPlayer = (currentPlayer + 1 == players.size()) ? 0 : currentPlayer + 1;

		if (!players.get(currentPlayer).isActive())
			nextPlayer();

	}

	private void moveRoll(int roll) {
		places.set(currentPlayer, places.get(currentPlayer) + roll);
		if (places.get(currentPlayer) > 11)
			places.set(currentPlayer, places.get(currentPlayer) - 12);

		System.out.println(players.get(currentPlayer) + "'s new location is " + places.get(currentPlayer));
		System.out.println("The category is " + currentCategory());
	}

	private void generateRandomQuestions() {
		for (int i = 0; i < 50; i++) {
			popQuestions.addLast(new PopQuestion(Integer.toString(i)));
			scienceQuestions.addLast(new ScienceQuestion(Integer.toString(i)));
			sportsQuestions.addLast(new SportsQuestion(Integer.toString(i)));
			rockQuestions.addLast(new RockQuestion(Integer.toString(i)));
		}
	}

	private void askQuestion() {
		switch (currentCategory()) {
		case POP:
			System.out.println(popQuestions.removeFirst());
			break;
		case SCIENCE:
			System.out.println(scienceQuestions.removeFirst());
			break;
		case SPORTS:
			System.out.println(sportsQuestions.removeFirst());
			break;
		case ROCK:
			System.out.println(rockQuestions.removeFirst());
			break;
		}
	}

	/**
	 * @return randomly category
	 */
	private Category currentCategory() {

		Category category = Category.ROCK;

		switch (places.get(currentPlayer)) {
		case 0:
		case 4:
		case 8:
			category = Category.POP;
			break;
		case 1:
		case 5:
		case 9:
			category = Category.SCIENCE;
			break;
		case 2:
		case 6:
		case 10:
			category = Category.SPORTS;
			break;
		default:
			category = Category.ROCK;
			break;
		}

		return category;
	}

	/**
	 * @return False if the last player won.
	 */
	private boolean didPlayerWin() {
		return !(purses.get(currentPlayer) == 6);
	}

	public static class SimpleSingleton {
		private static SimpleSingleton singleInstance = new SimpleSingleton();

		// Marking default constructor private
		// to avoid direct instantiation.
		private SimpleSingleton() {
		}

		// Get instance for class SimpleSingleton
		public static SimpleSingleton getInstance() {

			return singleInstance;
		}
	}

}
