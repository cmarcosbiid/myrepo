package com.adaptionsoft.games.uglytrivia.entity;

public class SportsQuestion extends Question {

	public SportsQuestion(String text) {
		super("Sports question: " + text);
	}

}
