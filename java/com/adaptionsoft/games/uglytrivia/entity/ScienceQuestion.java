package com.adaptionsoft.games.uglytrivia.entity;

public class ScienceQuestion extends Question {

	public ScienceQuestion(String text) {
		super("Science question: " + text);
	}

}
