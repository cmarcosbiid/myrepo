package com.adaptionsoft.games.uglytrivia.entity;

public class RockQuestion extends Question {

	public RockQuestion(String text) {
		super("Rock question: " + text);
	}

}
