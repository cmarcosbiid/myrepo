package com.adaptionsoft.games.uglytrivia.entity;

public class PopQuestion extends Question {

	public PopQuestion(String text) {
		super("Pop Question: " + text);
	}

}
