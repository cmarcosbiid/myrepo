package com.adaptionsoft.games.uglytrivia.entity;

public abstract class Question {

	private String text;

	public Question(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		return text;
	}

}
